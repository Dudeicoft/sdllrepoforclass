package pfsdll;

public class Node {

	private Integer _value;
	private Node _next;
	
	public Node(Integer value, Node next) {
		_value = value; _next = next;
	}
	
	public Integer value() {
		return _value;
	}
	
	public Node next() {
		return _next;
	}
	
	public void setValue(Integer val) {
		_value = val;
	}
	
	public void setNext(Node node) {
		_next = node;
	}
}
