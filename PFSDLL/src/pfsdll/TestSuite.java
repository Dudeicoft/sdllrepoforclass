package pfsdll;

import java.util.Random;

public class TestSuite {
	
	public TestSuite() {
		// ToDo
	}
	
	public void sequentialTest() {
		// Test creating empty lists
		System.out.println("Empty list: ");
		NodeList empty1 = new NodeList();
		empty1.print();
		System.out.print("\n");
		
		// Test with few elements in order entered
		System.out.println("Few element list: ");
		NodeList few1 = new NodeList(1,2,3,4,5,6,7,8,9,10);
		few1.print();
		System.out.print("\n");
		
		// Test with random elements
		System.out.println("Few random list: ");
		NodeList rand1 = new NodeList();
		Random rand = new Random();
		for (int i = 0; i < 8; i++) {
			Integer num = rand.nextInt(100);
			rand1 = rand1.insert(num);
		}
		rand1.print();
		System.out.print("\n");
		
		// Test deleting from empty
		System.out.println("Delete from empty: ");
		NodeList deleteEmpty = empty1.delete(5);
		deleteEmpty.print();
		System.out.print("\n");
		
		// Test find on empty
		System.out.println("Find on empty: ");
		empty1.find(5);
		empty1.print();
		System.out.print("\n");
		
		// Test delete on few elements
		System.out.println("Delete from few: ");
		NodeList deleteFew = few1.delete(5);
		deleteFew.print();
		System.out.print("\n");
		
		// Test find on known element
		System.out.println("Find on few (known): ");
		deleteFew.find(8);
		System.out.print("\n");
		
		// Test find on known gone element
		System.out.println("Find on few (gone): ");
		deleteFew.find(5);
		System.out.print("\n");
	}
	
	public void threeThreadTest() throws InterruptedException {
		System.out.println("Starting threes:");
		System.out.println("");
		NodeList rand2 = new NodeList();
		Random rand = new Random();
		for (int i = 0; i < 10; i++) {
			Integer num = rand.nextInt(10);
			rand2 = rand2.insert(num);
		}
		rand2.print();
		System.out.print("\n");
		
		// Create threads
		System.out.println("Starting threads...");
		Factory one = new Factory(rand2, 1, false);
		Factory two = new Factory(rand2, 2, false);
		Factory three = new Factory(rand2, 3, false);
		
		one.start(); two.start(); three.start();
		one.join(); two.join(); three.join();
	}
	
	public void growingThreadTest() throws InterruptedException {
		System.out.println("Starting growing:");
		System.out.println("");
		NodeList rand2 = new NodeList();
		Random rand = new Random();
		for (int i = 0; i < 10; i++) {
			Integer num = rand.nextInt(10);
			rand2 = rand2.insert(num);
		}
		rand2.print();
		System.out.print("\n");
		
		// Create threads
		System.out.println("Starting threads...");
		Factory one = new Factory(rand2, 1, true);
		Factory two = new Factory(rand2, 2, true);
		Factory three = new Factory(rand2, 3, true);
		
		one.start(); two.start(); three.start();
		one.join(); two.join(); three.join();
	}
	
	protected class Factory extends Thread {
		protected NodeList _nl;
		protected Integer _name;
		protected Boolean _grow;
		
		protected Factory(NodeList nl, Integer name, Boolean grow) {
			_nl = nl; _name = name; _grow = grow;
		}
		
		@Override
		public void run() {
			if (!_grow) {
				Random rand = new Random();
				Integer ready = rand.nextInt(10);
				System.out.println("Node " + _name + " inserting: " + ready);
				NodeList play = _nl.insert(ready);
				play.print();
				System.out.print("\n");
				System.out.print("Trying delete. ");
				play = play.delete(ready);
				play.print();
			} else {
				NodeList play = _nl;
				while (true) {
					Random rand = new Random();
					Integer ready = rand.nextInt(10);
					play = play.insert(ready);
					play.print();
					Integer rid = rand.nextInt(10);
					play = play.delete(rid);
					play.print();
				}
			}
		}
	}
}
