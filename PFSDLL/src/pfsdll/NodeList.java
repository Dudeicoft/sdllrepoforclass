package pfsdll;

public class NodeList {
	private Node _head;
	private Node _tail;
	private Integer _size;
	
//////////////////////////Constructors//////////////////////////
	
	public NodeList() {
		_head = null; _size = 0;
	}
	
	public NodeList(Integer... ints) {
		_head = null; _size = 0;
		for (Integer val: ints) {
			this.internalInsert(val);
		}
	}
	
//////////////////////////Property Functions//////////////////////////
	
	public Integer size() {
		return _size;
	}
	
	public void incSize() {
		_size += 1;
	}
	
	public void decSize() {
		_size -= 1;
	}
	
	public Node head() {
		return _head;
	}
	
	public void setHead(Node head) {
		_head = head;
	}
	
	private Node tail() {
		return _tail;
	}
	
	public void setTail(Node tail) {
		_tail = tail;
	}
	
//////////////////////////Internal Functions//////////////////////////
	
	public Boolean checkSorted() {
		Node curNode = this.head();
		Node prev = null;
		while (curNode != null) {
			if (curNode.next() == null) {
				// Sorted
				return true;
			} else {
				prev = curNode;
				curNode = curNode.next();
				if (prev.value() > curNode.value()) {
					// List is not sorted
					return false;
				}
			}
		}
		// Bypass means that there is no head so the list is sorted
		return true;
	}
	
	/*
	public Node copy(Node toCopy) {
		return new Node(toCopy.value(), toCopy.next());
	}
	*/
	
	public NodeList copy(NodeList toCopy) {
		if (toCopy.size() == 0) {
			return new NodeList();
		} else {
			NodeList tmp = new NodeList();
			Node cpHead = new Node(toCopy.head().value(), null);
			tmp.setHead(cpHead);
			tmp.setTail(cpHead);
			tmp.incSize();
			Node curNode = toCopy.head();
			while (curNode.next() != null) {
				curNode = curNode.next();
				tmp.insertAtEnd(curNode.value());
			}
			if (tmp.checkSorted())
				return tmp;
			else {
				System.out.println("There was an error while copying: final list not sorted");
				return null;
			}
		}
	}
	
	public void internalInsert(Integer val) {
		// Proceed along list until you find the insert point
		Node curNode = this.head();
		if (curNode == null) {
			Node newHead = new Node(val, null);
			this.setHead(newHead);
			this.setTail(newHead);
			this.incSize();
		} else if (val < curNode.value()) {
			this.setHead(new Node(val, this.head()));
			this.incSize();
		} else {
			Node prev = null;
			while (val > curNode.value()) {
				if (curNode.next() != null) {
					prev = curNode;
					curNode = curNode.next();
				}
				else {
					Node newNode = new Node(val, null);
					curNode.setNext(newNode);
					this.setTail(newNode);
					this.incSize();
					return;
				}
			}
			Node newNode = new Node(val, curNode);
			prev.setNext(newNode);
			this.incSize();
			if (this.checkSorted())
				return;
			else {
				System.out.println("There was an error while inserting: final list not sorted");
				return;
			}
		}
	}
	
	// Not sort safe, or persistent safe. Meant to be used internally.
	public void insertAtEnd(Integer val) {
		if (this.head() != null) {
			Node newTail = new Node(val, null);
			this.tail().setNext(newTail);
			this.setTail(newTail);
			this.incSize();
		} else {
			Node head = new Node(val, null);
			this.setHead(head);
			this.setTail(head);
			this.incSize();
		}
	}
	
	// Not sort safe, or persistent safe. Meant to be used internally. Might not get used
	/*private NodeList insertAtEnd(Node node) {
		list.tail().
	}*/
	
//////////////////////////External Functions//////////////////////////	
	
	public NodeList insert(Integer val) {
		System.out.println("Item inserting is: " + val);
		// Create copy of current list
		NodeList tmp = copy(this);
		// Proceed along list until you find the insert point
		Node curNode = tmp.head();
		if (curNode == null) {
			Node newHead = new Node(val, null);
			tmp.setHead(newHead);
			tmp.setTail(newHead);
			tmp.incSize();
			return tmp;
		} else if (val <= curNode.value()) {
			tmp.setHead(new Node(val, tmp.head()));
			tmp.incSize();
			return tmp;
		} else {
			Node prev = null;
			while (val > curNode.value()) {
				if (curNode.next() != null) {
					prev = curNode;
					curNode = curNode.next();
				}
				else {
					Node newNode = new Node(val, null);
					curNode.setNext(newNode);
					tmp.setTail(newNode);
					tmp.incSize();
					return tmp;
				}
			}
			Node newNode = new Node(val, curNode);
			prev.setNext(newNode);
			tmp.incSize();
			if (tmp.checkSorted())
				return tmp;
			else {
				System.out.println("There was an error while inserting: final list not sorted");
				return null;
			}
		}
	}
	
	public NodeList delete(Integer val) {
		// Create copy of current list
		NodeList tmp = copy(this);
		// Proceed along list until you find the delete point
		Node curNode = tmp.head();
		if (curNode == null) {
			System.out.println("The value is not within the list. List is empty");
			return this;
		} else if (val < curNode.value()) {
			System.out.println("The value is not within the list.");
			return this;
		} else if (val == curNode.value()) {
			tmp.setHead(curNode.next());
			tmp.decSize();
			return tmp;
		} else {
			Node prev = null;
			while (val > curNode.value()) {
				if (curNode.next() != null) {
					prev = curNode;
					curNode = curNode.next();
				}
				else {
					System.out.println("The value is not within the list mid.");
					return this;
				}
			}
			if (val == curNode.value()) {
				prev.setNext(curNode.next());
				tmp.decSize();
				if (tmp.checkSorted())
					return tmp;
				else {
					System.out.println("There was an error while deleting: final list not sorted");
					return null;
				}
			} else {
				System.out.println("The value is not within the list end.");
				return this;
			}
		}
	}
	
	public void find(Integer val) {
		// Proceed along list until you find the node
		Node curNode = this.head();
		if (curNode == null) {
			System.out.println("The value is not within the list. List is empty");
		} else if (val < curNode.value()) {
			System.out.println("The value is not within the list.");
		} else if (val == curNode.value()) {
			System.out.println("Found the value!");
		} else {
			while (val > curNode.value()) {
				if (curNode.next() != null) {
					curNode = curNode.next();
				} else {
					System.out.println("The value is not within the list.");
				}
			}
			if (val == curNode.value()) {
				System.out.println("Found the value!");
			} else {
				System.out.println("The value is not within the list.");
			}
		}	
	}
	
	public void print() {
		// Proceed along list and print;
		Node curNode = this.head();
		if (curNode == null) 
			System.out.println("Attempted to print but the list was empty.");
		else {
			System.out.println("<><>");
			System.out.println("Size: " + this.size());
			System.out.print("List: ");
			while (curNode != null) {
				System.out.print(curNode.value() + " ");
				curNode = curNode.next();
			}
			System.out.print("\n");
			System.out.println("<><>");
		}
	}
}