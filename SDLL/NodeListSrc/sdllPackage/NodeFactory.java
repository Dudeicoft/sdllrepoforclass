package sdllPackage;

import java.util.Random;

public class NodeFactory extends Thread {
	private int _numberToAdd;
	private NodeList _list;
	private int _storedToDelete;
	
	public NodeFactory(int num, NodeList list) {
		_numberToAdd = num; _list = list;
	}
	
	@Override
	public void run() {
		for(int i = 0; i < _numberToAdd; i++) {
			Random rand = new Random();
			int rep = rand.nextInt(10);
			int neg = rand.nextInt(1);
			try {
				_list.add(rep);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (neg % 2 == 0) {
				try {
					_list.add(-rep);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			if (i == 0) {
				_storedToDelete = rep;
			}
		}
		System.out.println("Deleting " + _storedToDelete);
		try {
			_list.remove(_storedToDelete);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}