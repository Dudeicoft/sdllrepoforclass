package sdllPackage;

import java.util.concurrent.Semaphore;

public class Node {
	private int _value;
	private int _count;
	private Node _left;
	private Node _right;
	// protected Semaphore rightHold = new Semaphore(1);
	// protected Semaphore leftHold = new Semaphore(1);
	protected Semaphore valueHold = new Semaphore(1);
	
	public Node(Node left, Node right, int value) {
		_value = value; _left = left; _right = right; _count = 1;
	}
	
	public int value() {
		return _value;
	}
	
	public int count() {
		return _count;
	}
	
	public void decCount() {
		_count--;
	}
	
	public void incCount() {
		_count++;
	}
	
	public Node right() {
		return _right;
	}
	
	public Node left() {
		return _left;
	}
	
	public void setRight(Node right) {
		_right = right;
	}
	
	public void setLeft(Node left) {
		_left = left;
	}
	
	public void setValue(int value) {
		_value = value;
	}
	
}