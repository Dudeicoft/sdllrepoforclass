package sdllPackage;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainClass {
	public static void main(String[] args) {
		int numToAdd;
		List<NodeFactory> nfList = new ArrayList<NodeFactory>();
		NodeList nList = new NodeList();
		if (args.length > 0) numToAdd = Integer.parseInt(args[0]);
		else numToAdd = 10;
		for(int i = 0; i < 5; i++) {
			NodeFactory nodeFactory = new NodeFactory(numToAdd, nList);
			nfList.add(nodeFactory);
		}
		for(NodeFactory nf : nfList) {
			nf.start();
		}
		for(NodeFactory nf : nfList) {
			try {
				nf.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nList.checkSorted();
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Enter a number to search for: ");
			String userInput = scanner.nextLine();
			if (userInput.equals("close")) break;
			int valToSearch = Integer.parseInt(userInput);
			try {
				nList.find(valToSearch);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		scanner.close();
	}
}