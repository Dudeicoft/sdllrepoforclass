package sdllPackage;

import java.util.concurrent.Semaphore;

public class NodeList {
	private Node _head;
	// private Node _tail;
	private int _size;
	protected static Semaphore _headHold = new Semaphore(1);
	
	public NodeList() {
		_head = null; /*_tail = null*/; _size = 0;
	}
	
	public boolean add(int value) throws InterruptedException {
		// Spider grabs head sem
		_headHold.acquire();
		// System.out.println("headHold grabbed for " + value);
		// Case for no head
		if (_head == null) {
			System.out.println("Head is null");
			_head = new Node(null, null, value);
			_size++;
			_headHold.release();
			// System.out.println("headHold released for " + value);
			return true;
		}
		_head.valueHold.acquire();
		// System.out.println("_headValueHold grabbed for " + value);
		_headHold.release();
		// System.out.println("headHold released for " + value);
		if (_head.value() == value) {
			// System.out.println("_head value same as " + value);
			_head.incCount();
			_head.valueHold.release();
			// System.out.println("_headValueHold released for " + value);
			return true;
		} else if (_head.value() > value) {
			// System.out.println("_head value greater than " + value + " so new head");
			Node tmp = new Node(null, _head, value);
			_head.setLeft(tmp);
			_head = tmp;
			_size++;
			tmp.right().valueHold.release();
			// System.out.println("_headValueHold released for " + value + " the previous head");
			return true;
		} else if (_head.value() < value) {
			// System.out.println("_head value less than " + value);
			if (_size == 1) {
				// System.out.println("size is one for new value " + value);
				Node tmp = new Node(_head, null, value);
				_head.setRight(tmp);
				_size++;
				_head.valueHold.release();
				// System.out.println("_headValueHold released for " + value);
				return true;
			} else {
				// System.out.println("size is not 1 for " + value);
				_head.right().valueHold.acquire();
				Node curNode = _head.right();
				while (curNode != null) {
					if (curNode.right() == null) {
						// System.out.println("End of line for " + value);
						if (curNode.value() == value) {
							// System.out.println("curNode same value as  " + value);
							curNode.incCount();
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						} else if (curNode.value() > value) {
							// System.out.println("curNode bigger than " + value);
							Node tmp = new Node(curNode.left(), curNode, value);
							curNode.left().setRight(tmp);
							curNode.setLeft(tmp);
							_size++;
							curNode.valueHold.release();
							tmp.left().valueHold.release();
							return true;
						} else if (curNode.value() < value) {
							// System.out.println("curNode smaller than " + value);
							Node tmp = new Node(curNode, null, value);
							curNode.setRight(tmp);
							_size++;
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						} else {
							// System.out.println("False hit for " + value);
							return false;
						}
					} else {
						curNode.right().valueHold.acquire();
						// System.out.println("right grabbed for " + value);
						if (curNode.value() == value) {
							// System.out.println("curNode same value as  " + value);
							curNode.incCount();
							curNode.right().valueHold.release();
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						} else if (curNode.value() > value) {
							// System.out.println("curNode bigger than " + value);
							Node tmp = new Node(curNode.left(), curNode, value);
							curNode.left().setRight(tmp);
							curNode.setLeft(tmp);
							_size++;
							curNode.right().valueHold.release();
							curNode.valueHold.release();
							tmp.left().valueHold.release();
							return true;
						} else if (curNode.value() < value) {
							// System.out.println("curNode smaller than " + value);
							curNode.left().valueHold.release();
							curNode = curNode.right();
						} else {
							// System.out.println("False hit for " + value);
							return false;
						}
					}
				}
			}
		} else {
			// System.out.println("hit false for " + value);
			return false;
		}
		return false;
	}
	
	public boolean remove(int value) throws InterruptedException {
		// Acquire head sem
		_headHold.acquire();
		// Check for case where there is no head
		if (_head == null) {
			System.out.println("There is no head in this list!");
			_headHold.release();
			return false;
		}
		// Head exists, check head
		_head.valueHold.acquire();
		if (_head.value() == value) {
			// replace remove head
			if (_head.count() - 1 <= 0) {
				_head.right().valueHold.acquire();
				Node tmp = _head.right();
				tmp.setLeft(null);
				_size--;
				tmp.valueHold.release();
				_head.valueHold.release();
				_head = tmp;
				_headHold.release();
				return true;
			} else {
				_head.decCount();
				_head.valueHold.release();
				_headHold.release();
				return true;
			}
		// Doesn't exist in the list
		} else if (_head.value() > value)  {
			System.out.println("This value is smaller than the smallest value in the list!");
			_head.valueHold.release();
			_headHold.release();
			return false;
		// Proceed to next item
		} else if (_head.value() < value) {
			_headHold.release();
			_head.right().valueHold.acquire();
			Node curNode = _head.right();
			while (curNode != null) {
				// last lapleft
				if (curNode.right() == null) {
					if (curNode.value() == value) {
						if (curNode.count() - 1 <= 0) {
							curNode.left().setRight(null);
							_size--;
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						} else {
							curNode.decCount();
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						}
					} else if (curNode.value() > value) {
						System.out.println("This value is not in the list!");
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return false;
					} else if (curNode.value() < value) {
						System.out.println("This value is not in the list!");
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return false;
					} else return false;
				} else {
					curNode.right().valueHold.acquire();
					if (curNode.value() == value) {
						if (curNode.count() - 1 <= 0) {
							curNode.left().setRight(curNode.right());
							curNode.right().setLeft(curNode.left());
							_size--;
							curNode.right().valueHold.release();
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						} else {
							curNode.decCount();
							curNode.right().valueHold.release();
							curNode.valueHold.release();
							curNode.left().valueHold.release();
							return true;
						}
					} else if (curNode.value() > value) {
						System.out.println("This value is not in the list!");
						curNode.right().valueHold.release();
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return false;
					} else if (curNode.value() < value) {
						// keep on moving
						curNode.left().valueHold.release();
						curNode = curNode.right();
					} else return false;
				}
			}
		}
		return false;
	}
	
	public boolean find(int value) throws InterruptedException {
		// Acquire head sem
		_headHold.acquire();
		// Check for case where there is no head
		if (_head == null) {
			System.out.println("There is no head in this list!");
			_headHold.release();
			return false;
		}
		// Head exists, check head
		_head.valueHold.acquire();
		if (_head.value() == value) {
			// return head and count
			System.out.println("Found " + value + " with count " + _head.count());
			_head.valueHold.release();
			_headHold.release();
			return true;
		// Doesn't exist in the list
		} else if (_head.value() > value)  {
			System.out.println("This " + value + " is smaller than the smallest value in the list!");
			_head.valueHold.release();
			_headHold.release();
			return false;
		// Proceed to next item
		} else if (_head.value() < value) {
			_headHold.release();
			_head.right().valueHold.acquire();
			Node curNode = _head.right();
			while (curNode != null) {
				// last lap
				if (curNode.right() == null) {
					if (curNode.value() == value) {
						System.out.println("Found " + value + " with count " + curNode.count());
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return true;
					} else if (curNode.value() > value) {
						System.out.println("This " + value + " is not in the list!");
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return false;
					} else if (curNode.value() < value) {
						System.out.println("This " + value + " is not in the list!");
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return false;
					} else return false;
				} else {
					curNode.right().valueHold.acquire();
					if (curNode.value() == value) {
						System.out.println("Found " + value + " with count " + curNode.count());
						curNode.right().valueHold.release();
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return true;
					} else if (curNode.value() > value) {
						System.out.println("This " + value + " is not in the list!");
						curNode.right().valueHold.release();
						curNode.valueHold.release();
						curNode.left().valueHold.release();
						return false;
					} else if (curNode.value() < value) {
						// keep on moving
						curNode.left().valueHold.release();
						curNode = curNode.right();
					} else return false;
				}
			}
		}
		return false;
	}
	
	public boolean checkSorted() {
		// Proceed right and check
		try {
			_headHold.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Node curNode = _head;
		System.out.println(curNode.value());
		while(curNode != null) {
			if (curNode.right() == null) curNode = null;
			else if (curNode.value() < curNode.right().value()) { 
				curNode = curNode.right();
				System.out.print(curNode.value() + ", ");
			} else if (curNode.value() == curNode.right().value()) {
				System.out.println("Messed up somehwere.");
				_headHold.release();
				return false;
			} else {
				System.out.println("Messed up somehwere.");
				_headHold.release();
				return false;
			}
		}
		System.out.println(_size);
		System.out.println("Everything looks good");
		_headHold.release();
		return true;
	}
}